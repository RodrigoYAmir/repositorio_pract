import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'
import {ICharacter} from './../../models/character'
import {Observable} from 'rxjs'

@Injectable({
  providedIn: 'root'
})
export class RymService {
  private URL = 'https://rickandmortyapi.com/api'
  //private URL = 'https://rickandmortyapi.com/api/character'//


  constructor(private httpCLient: HttpClient) { }
  //const url '$this.URL:/character () //

  getCharacters(): Observable<ICharacter[]> {
    return this.httpCLient.get<ICharacter[]>(`${this.URL}/character`)
  }
}
