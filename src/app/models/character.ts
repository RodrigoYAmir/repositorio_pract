export interface ICharacter {
status: string;
    name: string;
    image: string;
    gender: string;
    species: string;
}