import { style } from '@angular/animations';
import { Component, OnInit } from '@angular/core';
import {RymService} from './../../service/RickAndMorty/rym.service'
import {ICharacter} from './../../models/character'

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {// OnInit primero en ejecutarse//ng

  //public personajes: any=[]=[] / arreglo basio 
  characters: ICharacter[] = []
character: any;

  constructor(private rymService: RymService) {

  }

  ngOnInit() {
    this.rymService.getCharacters().subscribe((response: any) => {
      this.characters = response.results;
    })
  }


}

